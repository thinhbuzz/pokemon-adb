package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"os/exec"
)

type Message struct {
	Lat string `json:"lat"`
	Lng string `json:"lng"`
}
type ResponseMessage struct {
	Command string `json:"command"`
}

func Cleaner(w http.ResponseWriter, r *http.Request) {
	b, err := ioutil.ReadAll(r.Body)
	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}

	var msg Message
	err = json.Unmarshal(b, &msg)
	if err != nil {
		http.Error(w, err.Error(), 500)
		return
	}
	cmd := exec.Command("adb", "shell", "am", "start-foreground-service", "-a", "theappninjas.gpsjoystick.TELEPORT", "--ef", "lat", msg.Lat, "--ef", "lng", msg.Lng)
	commandOutput, _ := cmd.CombinedOutput()
	fmt.Println(string(commandOutput))

	w.Header().Set("Access-Control-Allow-Origin", "*")
	w.Header().Set("Access-Control-Allow-Headers", "Content-Type")
	responseMessage := ResponseMessage{Command: string(commandOutput)}
	b, err = json.Marshal(responseMessage)

	w.Write(b)
}

func main() {
	http.HandleFunc("/adb", Cleaner)
	port := "4444"
	if len(os.Args) > 1 && os.Args[1] != "" {
		port = os.Args[1]
		log.Println("port", port)
	}
	address := ":" + port
	log.Println("Starting server on address", address)
	err := http.ListenAndServe(address, nil)
	if err != nil {
		panic(err)
	}
}
